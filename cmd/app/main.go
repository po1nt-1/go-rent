package main

import (
	"go-rent/config"
	"go-rent/internal/app"
	"log"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatalf("Config error: %v", err)
	}

	app.Run(cfg)
}
