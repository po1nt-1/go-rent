package app

import (
	"go-rent/config"
	"log"
	"time"
)

func Run(cfg *config.Config) {
	for {
		log.Printf("%v running", cfg.App.Name)
		time.Sleep(3 * time.Second)
	}
}
